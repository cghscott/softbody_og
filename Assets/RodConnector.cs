using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    public class RodConnector : BaseConnector
    {
        private float _combinedMass;

        internal override void getForce(bool isVerlet)
        {
            if (isForceless == false)
            {
                switch (isPartialStatic, isVerlet)
                {
                    case (true, false):
                        RodForcePartialStaticInitial();
                        break;
                    case (true, true):
                        RodForcePartialStaticVerlet();
                        break;
                    case (false, false):
                        RodForceDynamicInitial();
                        break;
                    case (false, true):
                        RodForceDynamicVerlet();
                        break;
                }
            }
        }

        private void RodForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float Overshoot = pointToPoint.magnitude - length;
            // multiplied by 2 due to half timestep of acceleration but it's a whole timestep of velocity so it isn't Mathf.Pow(fixedDeltaTime / 2)
            Vector2 accelerationComponent = 2 * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent);
        }

        private void RodForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float Overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent = 2 * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent);
        }

        private void RodForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float Overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent1 = 2 * endPoint.properties.mass / _combinedMass * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * startPoint.properties.mass / _combinedMass * Overshoot * -pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.accelerationRigidComponents.Add(accelerationComponent2);
        }

        private void RodForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float Overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent1 = 2 * endPoint.properties.mass / _combinedMass * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * startPoint.properties.mass / _combinedMass * Overshoot * -pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.accelerationRigidComponents.Add(accelerationComponent2);
        }

        public RodConnector(PhysicsObject startPointInput, PhysicsObject endPointInput, float lengthInput) : base(startPointInput, endPointInput, lengthInput)
        {
            _combinedMass = startPointInput.properties.mass + endPointInput.properties.mass;
        }
    }
}

