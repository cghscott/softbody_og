using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    public class SpringConnector : BaseConnector
    {
        private float _springConstant;
        private float _springDamping;

        internal override void getForce(bool isVerlet)
        {
            if (isForceless == false)
            {
                switch (isPartialStatic, isVerlet)
                {
                    case (true, false):
                        SpringForcePartialStaticInitial();
                        SpringDragForcePartialStaticInitial();
                        break;
                    case (true, true):
                        SpringForcePartialStaticVerlet();
                        SpringDragForcePartialStaticVerlet();
                        break;
                    case (false, false):
                        SpringForceDynamicInitial();
                        SpringDragForceDynamicInitial();
                        break;
                    case (false, true):
                        SpringForceDynamicVerlet();
                        SpringDragForceDynamicVerlet();
                        break;
                }
            }
        }

        private void SpringForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent;
        }

        private void SpringForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent;
        }

        private void SpringForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent1 = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springConstant * overshoot * -pointToPoint.normalized / endPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent1;
            endPoint.verletData.accelerationInitial += accelerationComponent2;
        }

        private void SpringForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 accelerationComponent1 = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springConstant * overshoot * -pointToPoint.normalized / endPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent1;
            endPoint.kinematicData.acceleration += accelerationComponent2;
        }

        private void SpringDragForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float relativeAngle = Vector2.Angle(startPoint.kinematicDataOld.velocity, pointToPoint);
            float relativeAngleRadians = relativeAngle * Mathf.PI / 180;
            Vector2 accelerationComponent = _springDamping * -Mathf.Cos(relativeAngleRadians) * startPoint.kinematicDataOld.velocity.magnitude 
                * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent;
        }

        private void SpringDragForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float relativeAngle = Vector2.Angle(startPoint.verletData.halfstepVelocity, pointToPoint);
            float relativeAngleRadians = relativeAngle * Mathf.PI / 180;
            Vector2 accelerationComponent = _springDamping * -Mathf.Cos(relativeAngleRadians) * startPoint.verletData.halfstepVelocity.magnitude 
                * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent;
        }

        private void SpringDragForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            Vector2 velocityRelative = endPoint.kinematicDataOld.velocity - startPoint.kinematicDataOld.velocity;
            float relativeAngle = Vector2.Angle(velocityRelative, pointToPoint);
            float relativeAngleRadians = relativeAngle * Mathf.PI / 180;
            Vector2 accelerationComponent1 = _springDamping * Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springDamping * -Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / endPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent1;
            endPoint.verletData.accelerationInitial += accelerationComponent2;
        }

        private void SpringDragForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            Vector2 velocityRelative = endPoint.kinematicDataOld.velocity - startPoint.kinematicDataOld.velocity;
            float relativeAngle = Vector2.Angle(velocityRelative, pointToPoint);
            float relativeAngleRadians = relativeAngle * Mathf.PI / 180;
            Vector2 accelerationComponent1 = _springDamping * Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springDamping * -Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / endPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent1;
            endPoint.kinematicData.acceleration += accelerationComponent2;
        }

        public SpringConnector(PhysicsObject startPointInput, PhysicsObject endPointInput, float lengthInput, float springConstantInput, float springDampingInput)
            : base(startPointInput, endPointInput, lengthInput)
        {
            _springConstant = springConstantInput;
            _springDamping = springDampingInput;
        }
    }
}