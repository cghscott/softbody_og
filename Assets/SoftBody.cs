using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public class SoftBody
    {
        private List<PhysicsObject> _nodes;
        private List<Connectors.BaseConnector> _connectors;
        private float _lineWidth;

        public SoftBody(List<PhysicsObject> nodes, List<Connectors.BaseConnector> connectors, float lineWidth)
        {
            _nodes = nodes;
            _connectors = connectors;
            _lineWidth = lineWidth;
        }

        internal void RefreshData()
        {
            foreach (PhysicsObject node in _nodes)
            {
                node.RefreshData();
            }
        }

        internal void CalculateAccelerations(bool isVerlet)
        {
            // Forces applied directlky to all dynamic points and RigidBodies
            foreach (PhysicsObject node in _nodes)
            {
                node.GetForce(isVerlet);
            }

            // Forces are applied for all springs and bungees and calculated for rods and ropes
            foreach (Connectors.BaseConnector connector in _connectors)
            {
                connector.getForce(isVerlet);
            }

            // Averages and applies force components from ropes and rods attached to each point
            foreach (PhysicsObject node in _nodes)
            {
                node.ResolveRigidForces(isVerlet);
            }

            // force due to collisions is applied
            foreach (PhysicsObject node in _nodes)
            {
                node.CollisionForce(isVerlet);
            }
        }

        // Start of Verlet Step

        internal void CalculatePseudoEndPositions()
        {
            foreach (PhysicsObject node in _nodes)
            {
                node.CalculatePseudoEndPosition();
            }
        }

        // Probably have to loop everthing before this for all bodies before moving on from pseudo positions

        internal void CalculateEndPositions()
        {
            foreach (PhysicsObject node in _nodes)
            {
                node.CalculateRealEndPosition();
            }
        }

        internal void DrawLines()
        {
            foreach (Connectors.BaseConnector connector in _connectors)
            {
                GameObject myLine = new GameObject();
                myLine.transform.position = connector.startPoint.kinematicData.position;
                myLine.AddComponent<LineRenderer>();

                LineRenderer lr = myLine.GetComponent<LineRenderer>();
                lr.widthMultiplier = _lineWidth;
                lr.material = new Material(Shader.Find("Sprites/Default"));
                lr.SetPosition(0, connector.startPoint.kinematicData.position);
                lr.SetPosition(1, connector.endPoint.kinematicData.position);

                GameObject.Destroy(myLine, Time.fixedDeltaTime);
            }
            foreach (PhysicsObject node in _nodes)
            {
                node.DrawObject();
            }
        }
    }
}
