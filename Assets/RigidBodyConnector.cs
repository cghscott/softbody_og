using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    internal class RigidBodyConnector : BaseConnector
    {
        private float _combinedMass;
        private float _relativeAngle;

        // Need to do something about the endPoint here
        internal RigidBodyConnector(PhysicsObject startPoint, PhysicsObject endPoint, float length, float angleInitial, bool fixedAngleGlobal) 
            : base(startPoint, endPoint, length)
        {
            _relativeAngle = angleInitial - endPoint.kinematicData.orientation;
            _combinedMass = endPoint.properties.mass + startPoint.properties.mass;
        }

        internal override void getForce(bool isVerlet)
        {
            if (isVerlet == false)
            {
                RigidBodyConnectorForceInitial();
            }
            else
            {
                RigidBodyConnectorForceVerlet();
            }
        }

        private void RigidBodyConnectorForceInitial()
        {
            // Force along connection
            Vector2 pointToCentreOfMass = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToCentreOfMass.magnitude - length;
            Vector2 accelerationComponent1 = 2 * (endPoint.properties.mass / _combinedMass) * overshoot * pointToCentreOfMass.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * (startPoint.properties.mass / _combinedMass) * overshoot * -pointToCentreOfMass.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);

            // Force to maintain angle
            float angleIntended = endPoint.kinematicDataOld.orientation + _relativeAngle;
            float angleIntendedRadians = angleIntended * Mathf.PI / 180;
            float angleReal = Vector2.SignedAngle(Vector2.right, -pointToCentreOfMass.normalized);
            float angleRealRadians = angleReal * Mathf.PI / 180;
            accelerationComponent1.x += (endPoint.properties.mass / _combinedMass) * (Mathf.Cos(angleIntendedRadians) - Mathf.Cos(angleRealRadians)) * length;
            accelerationComponent1.y += (endPoint.properties.mass / _combinedMass) * (Mathf.Sin(angleIntendedRadians) - Mathf.Sin(angleRealRadians)) * length;
            accelerationComponent2.x += (startPoint.properties.mass / _combinedMass) * (Mathf.Cos(angleIntendedRadians + Mathf.PI) - Mathf.Cos(angleRealRadians + Mathf.PI)) * length;
            accelerationComponent2.y += (startPoint.properties.mass / _combinedMass) * (Mathf.Sin(angleIntendedRadians + Mathf.PI) - Mathf.Sin(angleRealRadians + Mathf.PI)) * length;

            // Torque due to linear motion

            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.verletData.accelerationInitial += accelerationComponent2;
        }

        private void RigidBodyConnectorForceVerlet()
        {
            // Force along connection
            Vector2 pointToCentreOfMass = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToCentreOfMass.magnitude - length;
            Vector2 accelerationComponent1 = 2 * (endPoint.properties.mass / _combinedMass) * overshoot * pointToCentreOfMass.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * (startPoint.properties.mass / _combinedMass) * overshoot * -pointToCentreOfMass.normalized  / Mathf.Pow(Time.fixedDeltaTime, 2);

            // Force to maintain angle
            float angleIntended = endPoint.verletData.pseudoEndOrientation + _relativeAngle;
            float angleIntendedRadians = angleIntended * Mathf.PI / 180;
            float angleReal = Vector2.SignedAngle(Vector2.right, -pointToCentreOfMass.normalized);
            float angleRealRadians = angleReal * Mathf.PI / 180;
            accelerationComponent1.x += (endPoint.properties.mass / _combinedMass) * (Mathf.Cos(angleIntendedRadians) - Mathf.Cos(angleRealRadians)) * length;
            accelerationComponent1.y += (endPoint.properties.mass / _combinedMass) * (Mathf.Sin(angleIntendedRadians) - Mathf.Sin(angleRealRadians)) * length;
            accelerationComponent2.x += (startPoint.properties.mass / _combinedMass) * (Mathf.Cos(angleIntendedRadians + Mathf.PI) - Mathf.Cos(angleRealRadians + Mathf.PI)) * length;
            accelerationComponent2.y += (startPoint.properties.mass / _combinedMass) * (Mathf.Sin(angleIntendedRadians + Mathf.PI) - Mathf.Sin(angleRealRadians + Mathf.PI)) * length;

            // Torque due to linear motion

            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.kinematicData.acceleration += accelerationComponent2;
        }



        // Currently built as a final adjustment at the end of each step
        internal void VelocityAngularConversionPseudoStep()
        {
            Vector2 pointToCentreOfMass = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            Vector2 momentFromAngular = endPoint.verletData.halfstepVelocityAngular * endPoint.properties.inertia * Mathf.Sign(endPoint.verletData.halfstepVelocityAngular) 
                * Vector2.Perpendicular(pointToCentreOfMass) / length;
            Vector2 velocityAdjust = momentFromAngular / startPoint.properties.mass;

            Vector2 relativeVelocity = startPoint.verletData.halfstepVelocity - endPoint.verletData.halfstepVelocity;
            float relativeAngle = Vector2.SignedAngle(pointToCentreOfMass, relativeVelocity);
            float angularMomentAdjust = relativeVelocity.magnitude * Mathf.Sin(relativeAngle) * startPoint.properties.mass * length;
            float velocityAngularAdjust = angularMomentAdjust / endPoint.properties.inertia;

            startPoint.verletData.halfstepVelocity += velocityAdjust;
            endPoint.verletData.halfstepVelocityAngular += velocityAngularAdjust;
        }

        internal void VelocityAngularConversionFinal()
        {
            Vector2 pointToCentreOfMass = endPoint.kinematicData.position - startPoint.kinematicData.position;
            Vector2 momentFromAngular = endPoint.kinematicData.velocityAngular * endPoint.properties.inertia * Mathf.Sign(endPoint.kinematicData.velocityAngular)
                * Vector2.Perpendicular(pointToCentreOfMass) / length;
            Vector2 velocityAdjust = momentFromAngular / startPoint.properties.mass;

            Vector2 relativeVelocity = startPoint.kinematicData.velocity - endPoint.kinematicData.velocity;
            float relativeAngle = Vector2.SignedAngle(pointToCentreOfMass, relativeVelocity);
            float angularMomentAdjust = relativeVelocity.magnitude * Mathf.Sin(relativeAngle) * startPoint.properties.mass * length;
            float velocityAngularAdjust = angularMomentAdjust / endPoint.properties.inertia;

            startPoint.kinematicData.velocity += velocityAdjust;
            endPoint.kinematicData.velocityAngular += velocityAngularAdjust;
        }
    }
}
