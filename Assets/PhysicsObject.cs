using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public struct KinematicDataRigidBody
    {
        public Vector2 position;
        public float orientation;
        public Vector2 velocity;
        public float velocityAngular;
        public Vector2 acceleration;
        public float accelerationRotational;
    }

    internal struct VerletDataRigidBody
    {
        internal Vector2 pseudoEndPosition;
        internal float pseudoEndOrientation;
        internal Vector2 halfstepVelocity;
        internal float halfstepVelocityAngular;
        internal Vector2 accelerationInitial;
        internal float accelerationInitialRotational;
    }

    internal struct Properties
    {
        internal float mass;
        internal float inertia;
        internal float gravity;
        internal float aerialDragCoefficient;
        internal bool isStatic;
    }

    public abstract class PhysicsObject
    {
        public KinematicDataRigidBody kinematicData;
        protected internal KinematicDataRigidBody kinematicDataOld { get; protected set; }
        internal VerletDataRigidBody verletData;
        internal List<Vector2> accelerationRigidComponents;
        internal Properties properties;

        public abstract void RefreshData();
        public abstract void GetForce(bool isVerlet);
        internal abstract void ResolveRigidForces(bool isVerlet);
        internal abstract void CollisionForce(bool isVerlet);
        internal abstract void CalculatePseudoEndPosition();
        internal abstract void CalculateRealEndPosition();
        internal abstract void DrawObject();
        protected PhysicsObject(Vector2 position, bool isStatic, float mass, float gravity, float aerialDragCoefficient)
        {
            kinematicData.position = position;
            properties.isStatic = isStatic;
            properties.mass = mass;
            properties.gravity = gravity;
            properties.aerialDragCoefficient = aerialDragCoefficient;
            accelerationRigidComponents = new List<Vector2>();

        }
    }
}
