using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    public class RopeConnector : BaseConnector
    {
        private float _combinedMass;

        internal override void getForce(bool isVerlet)
        {
            if (isForceless == false)
            {
                switch (isPartialStatic, isVerlet)
                {
                    case (true, false):
                        RopeForcePartialStaticInitial();
                        break;
                    case (true, true):
                        RopeForcePartialStaticVerlet();
                        break;
                    case (false, false):
                        RopeForceDynamicInitial();
                        break;
                    case (false, true):
                        RopeForceDynamicVerlet();
                        break;
                }
            }
        }

        private void RopeForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float Overshoot = pointToPoint.magnitude - length;
            if (Overshoot < 0)
            {
                Overshoot = 0;
            }
            Vector2 accelerationComponent = 2 * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent);
        }

        private void RopeForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float Overshoot = pointToPoint.magnitude - length;
            if (Overshoot < 0)
            {
                Overshoot = 0;
            }
            Vector2 accelerationComponent = 2 * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent);
        }

        private void RopeForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float Overshoot = pointToPoint.magnitude - length;
            if (Overshoot < 0)
            {
                Overshoot = 0;
            }
            Vector2 accelerationComponent1 = 2 * endPoint.properties.mass / _combinedMass * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * startPoint.properties.mass / _combinedMass * Overshoot * -pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.accelerationRigidComponents.Add(accelerationComponent2);
        }

        private void RopeForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float Overshoot = pointToPoint.magnitude - length;
            if (Overshoot < 0)
            {
                Overshoot = 0;
            }
            Vector2 accelerationComponent1 = 2 * endPoint.properties.mass / _combinedMass * Overshoot * pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            Vector2 accelerationComponent2 = 2 * startPoint.properties.mass / _combinedMass * Overshoot * -pointToPoint.normalized / Mathf.Pow(Time.fixedDeltaTime, 2);
            startPoint.accelerationRigidComponents.Add(accelerationComponent1);
            endPoint.accelerationRigidComponents.Add(accelerationComponent2);
        }

        public RopeConnector(PhysicsObject startPointInput, PhysicsObject endPointInput, float lengthInput) : base(startPointInput, endPointInput, lengthInput)
        {
            _combinedMass = startPointInput.properties.mass + endPointInput.properties.mass;
        }
    }
}