using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    public abstract class BaseConnector
    {
        public PhysicsObject startPoint;
        public PhysicsObject endPoint;
        protected float length { get; private set; }
        protected bool isPartialStatic { get; private set; } = false;
        internal bool isForceless { get; private set; } = false;

        internal abstract void getForce(bool isVerlet);

        protected BaseConnector(PhysicsObject startPointInput, PhysicsObject endPointInput, float lengthInput)
        {

            startPoint = startPointInput;
            endPoint = endPointInput;
            length = lengthInput;

            if (startPoint.properties.isStatic == true && endPoint.properties.isStatic == true)
            {
                isForceless = true;
            }

            else if (endPoint.properties.isStatic == true)
            {
                isPartialStatic = true;
            }

            else if (startPoint.properties.isStatic == true)
            {
                isPartialStatic = true;
                // Ensures that the start point is always dynamic for partialStatic connectors
                startPoint = endPointInput;
                endPoint = startPointInput;
            }

            // negative length values automatically default to the initial separation of the points
            if (length < 0)
            {
                Vector2 pointToPoint = endPoint.kinematicData.position - startPoint.kinematicData.position;
                length = pointToPoint.magnitude;
            }
        }
    }
}

