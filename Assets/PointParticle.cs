using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public class PointParticle : PhysicsObject
    {
        private float _bounceRatio;
        private LayerMask _mask;

        public PointParticle(Vector2 position, bool isStatic, float mass, float gravity, float aerialDragCoefficient, float bounceRatio, LayerMask mask) 
            : base(position, isStatic, mass, gravity, aerialDragCoefficient)
        {
            _bounceRatio = bounceRatio;
            _mask = mask;

            kinematicDataOld = kinematicData;
        }

        public override void RefreshData()
        {
            kinematicDataOld = kinematicData;
            //kinematicData = new KinematicDataParticle();        - This might be more robust to future editing than the following
            verletData.accelerationInitial = Vector2.zero;
            kinematicData.acceleration = Vector2.zero;
        }

        public override void GetForce(bool isVerlet)
        {
            if (properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                    GravityForceInitial();
                    AerialDragInitial();
                }
                else
                {
                    GravityForceVerlet();
                    AerialDragVerlet();
                }
            }
        }

        private void GravityForceInitial()
        {
            verletData.accelerationInitial.y += properties.gravity;
        }

        private void GravityForceVerlet()
        {
            kinematicData.acceleration.y += properties.gravity;
        }

        private void AerialDragInitial()
        {
            Vector2 accelerationDragComponent = properties.aerialDragCoefficient * Mathf.Pow(kinematicDataOld.velocity.magnitude, 2) 
                * -kinematicDataOld.velocity.normalized / properties.mass;
            verletData.accelerationInitial += accelerationDragComponent;
        }

        private void AerialDragVerlet()
        {
            Vector2 accelerationDragComponent = properties.aerialDragCoefficient * Mathf.Pow(verletData.halfstepVelocity.magnitude, 2) 
                * -verletData.halfstepVelocity.normalized / properties.mass;
            kinematicData.acceleration += accelerationDragComponent;
        }

        internal override void ResolveRigidForces(bool isVerlet)
        {
            if (accelerationRigidComponents.Count != 0 && properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                    RigidResolveInitial();
                }
                else
                {
                    RigidResolveVerlet();
                }
            }

            accelerationRigidComponents = new List<Vector2>();
        }

        private void RigidResolveInitial()
        {
            Vector2 componentsTotal = Vector2.zero;
            Vector2 componentsAverage;
            foreach (Vector2 component in accelerationRigidComponents)
            {
                componentsTotal += component;
            }
            componentsAverage = componentsTotal / accelerationRigidComponents.Count;
            verletData.accelerationInitial += componentsAverage;
        }

        private void RigidResolveVerlet()
        {
            Vector2 componentsTotal = Vector2.zero;
            Vector2 componentsAverage;
            foreach (Vector2 component in accelerationRigidComponents)
            {
                componentsTotal += component;
            }
            componentsAverage = componentsTotal / accelerationRigidComponents.Count;
            kinematicData.acceleration += componentsAverage;
        }

        internal override void CollisionForce(bool isVerlet)
        {
            if (properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                    PointCollisionForceInitial();
                }
                else
                {
                    PointCollisionForceVerlet();
                }
            }
        }

        private void PointCollisionForceInitial()
        {
            Vector2 pseudoVelocity = kinematicDataOld.velocity + 0.5f * verletData.accelerationInitial * Time.fixedDeltaTime;
            float rayLength = pseudoVelocity.magnitude * Time.fixedDeltaTime;
            RaycastHit2D hit = Physics2D.Raycast(kinematicDataOld.position, pseudoVelocity, rayLength, _mask);
            if (hit)
            {
                float angle = Vector2.Angle(pseudoVelocity, hit.normal);
                float overshoot = rayLength - hit.distance;
                float depth = overshoot * Mathf.Cos(angle);
                kinematicData.acceleration += 4 * (1 + _bounceRatio) * depth * -hit.normal / Mathf.Pow(Time.fixedDeltaTime, 2);
            }
        }

        private void PointCollisionForceVerlet()
        {
            Vector2 pseudoVelocity = verletData.halfstepVelocity + 0.5f * kinematicData.acceleration * Time.fixedDeltaTime;
            float rayLength = pseudoVelocity.magnitude * Time.fixedDeltaTime;
            RaycastHit2D hit = Physics2D.Raycast(verletData.pseudoEndPosition, pseudoVelocity, rayLength, _mask);
            if (hit)
            {
                float angle = Vector2.Angle(pseudoVelocity, hit.normal);
                float overshoot = rayLength - hit.distance;
                float depth = overshoot * Mathf.Cos(angle);
                kinematicData.acceleration += 4 * (1 + _bounceRatio) * depth * -hit.normal / Mathf.Pow(Time.fixedDeltaTime, 2);
            }
        }

        internal override void CalculatePseudoEndPosition()
        {
            verletData.halfstepVelocity = kinematicDataOld.velocity + 0.5f * verletData.accelerationInitial * Time.fixedDeltaTime;
            verletData.pseudoEndPosition = kinematicDataOld.position + verletData.halfstepVelocity * Time.fixedDeltaTime;
        }

        internal override void CalculateRealEndPosition()
        {
            kinematicData.velocity = verletData.halfstepVelocity + 0.5f * kinematicData.acceleration * Time.fixedDeltaTime;
            kinematicData.position = kinematicDataOld.position + kinematicData.velocity * Time.fixedDeltaTime;
            kinematicData.acceleration = (kinematicData.velocity - kinematicDataOld.velocity) / Time.fixedDeltaTime;
        }

        internal override void DrawObject()
        {
        }
    }
}