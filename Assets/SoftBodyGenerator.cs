using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public static class SoftBodyGenerator
    {
        public static SoftBody GenerateSoftBody(float massGlobal, float gravityGlobal, float airResistanceGlobal, 
            float bounceRatioGlobal, LayerMask mask, float ropeLength, float ropeSpringStrength, float springDamping, float lineWidth)
        {
            List<PhysicsObject> nodes;
            nodes = new List<PhysicsObject>();
            CreatePoints(nodes, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask);

            List<RigidBodyMotion> rigidBodies;
            rigidBodies = new List<RigidBodyMotion>();
            CreateRigidBodies(nodes, massGlobal, gravityGlobal, airResistanceGlobal);

            List<Connectors.BaseConnector> connectors;
            connectors = new List<Connectors.BaseConnector>();
            CreateConnectors(connectors, nodes, ropeLength, ropeSpringStrength, springDamping);

            return new SoftBody(nodes, connectors, lineWidth);
        }

        private static void CreatePoints(List<PhysicsObject> nodes, float massGlobal, float gravityGlobal, float airResistanceGlobal, float bounceRatioGlobal, LayerMask mask)
        {
            Vector2 position3 = new Vector2(1, 0);
            nodes.Add(new PointParticle(position3, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position4 = new Vector2(1, 1);
            nodes.Add(new PointParticle(position4, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position5 = new Vector2(0, 1);
            nodes.Add(new PointParticle(position5, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position6 = new Vector2(0, 0);
            nodes.Add(new PointParticle(position6, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));

            Vector2 positionStatic = new Vector2(-0.75f, 0.95f);
            nodes.Add(new PointParticle(positionStatic, true, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));

            Vector2 position7 = new Vector2(-1, -2);
            nodes.Add(new PointParticle(position7, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position8 = new Vector2(-1, -1);
            nodes.Add(new PointParticle(position8, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position9 = new Vector2(-2, -1);
            nodes.Add(new PointParticle(position9, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
            Vector2 position10 = new Vector2(-2, -2);
            nodes.Add(new PointParticle(position10, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));

            Vector2 position11 = new Vector2(-1.5f, 0f);
            nodes.Add(new PointParticle(positionStatic, false, massGlobal, gravityGlobal, airResistanceGlobal, bounceRatioGlobal, mask));
        }

        private static void CreateRigidBodies(List<PhysicsObject> nodes, float massGlobal, float gravityGlobal, float airResistanceGlobal)
        {
            Vector2 position = new Vector2(-2, -4);
            nodes.Add(new RigidBodyMotion(position, false, massGlobal, gravityGlobal, 45f, airResistanceGlobal, airResistanceGlobal / 2, 4f, 2f, 0.2f));
        }

        private static void CreateConnectors(List<Connectors.BaseConnector> connectors, List<PhysicsObject> nodes, float ropeLength, float ropeSpringStrength, float springDamping)
        {
            connectors.Add(new Connectors.BungeeConnector(nodes[4], nodes[0], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[0], nodes[1], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[1], nodes[2], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[2], nodes[3], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[3], nodes[0], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[1], nodes[3], -1f, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.SpringConnector(nodes[2], nodes[0], -1f, ropeSpringStrength, springDamping));

            connectors.Add(new Connectors.RopeConnector(nodes[4], nodes[5], -1f));
            connectors.Add(new Connectors.RodConnector(nodes[5], nodes[6], ropeLength));
            connectors.Add(new Connectors.RodConnector(nodes[6], nodes[7], ropeLength));
            connectors.Add(new Connectors.RodConnector(nodes[7], nodes[8], ropeLength));
            connectors.Add(new Connectors.RodConnector(nodes[8], nodes[5], ropeLength));
            connectors.Add(new Connectors.RopeConnector(nodes[6], nodes[8], -1f));
            connectors.Add(new Connectors.RopeConnector(nodes[7], nodes[5], -1f));

            //connectors.Add(new Connectors.RodConnector(nodes[0], nodes[1], ropeLength));
            //connectors.Add(new Connectors.RopeConnector(nodes[0], nodes[2], ropeLength));
            //connectors.Add(new Connectors.SpringConnector(nodes[0], nodes[3], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.BungeeConnector(nodes[4], nodes[9], ropeLength, ropeSpringStrength, springDamping));
            connectors.Add(new Connectors.RigidBodyConnector(nodes[9], nodes[10], ropeLength, 45, false));
        }

    }
}