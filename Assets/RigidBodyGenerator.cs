using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    internal static class RigidBodyGenerator
    {
        internal static RigidBodyMotion GenerateRigidBody(Vector2 position, float orientationAngleDegrees, float mass, float aerialDragCoefficient, 
            float angularDragCoefficient, float width, float height, float lineWidth)
        {
            Rigidbody2D rigidBody = new Rigidbody2D();
            rigidBody.isKinematic = true;
            rigidBody.position = position;
            rigidBody.rotation = orientationAngleDegrees;
            rigidBody.mass = mass;
            rigidBody.drag = aerialDragCoefficient;
            rigidBody.angularDrag = angularDragCoefficient;
            float gravity = 1000000000f;

            RigidBodyMotion rigidBodyMotion = new RigidBodyMotion(position, false, mass, gravity, orientationAngleDegrees, aerialDragCoefficient,
            angularDragCoefficient, width, height, lineWidth);

            return rigidBodyMotion;
        }
    }
}
