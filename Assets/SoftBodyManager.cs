using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public class SoftBodyManager : MonoBehaviour
    {
        [SerializeField] private LayerMask _collisionMask;
        [SerializeField] private float _gravityGlobal = -9.8f;
        [SerializeField] private float _ropeLength = 7;
        [SerializeField] private float _ropeSpringStrength = 1;
        [SerializeField] private float _massGlobal = 10;
        [SerializeField] private float _springDamping = 0.2f;
        [SerializeField] private float _airResistanceGlobal = 0.5f;
        [SerializeField] private float _lineWidth = 0.1f;
        [SerializeField] private float _bounceRatioGlobal = 0.3f;

        private List<SoftBody> _softBodies;
        private List<RigidBodyMotion> _rigidBodies;
        
        void Start()
        {
            _softBodies = new List<SoftBody>();
            _softBodies.Add(SoftBodyGenerator.GenerateSoftBody(_massGlobal, _gravityGlobal, _airResistanceGlobal, _bounceRatioGlobal, _collisionMask,
                _ropeLength, _ropeSpringStrength, _springDamping, _lineWidth));
        }

        void FixedUpdate()
        {
            // Step 1
            foreach (SoftBody softBody in _softBodies)
            {
                softBody.RefreshData();
                softBody.CalculateAccelerations(false);
                softBody.CalculatePseudoEndPositions();
            }

            // Step 2
            foreach (SoftBody softBody in _softBodies)
            {
                softBody.CalculateAccelerations(true);
                softBody.CalculateEndPositions();
                softBody.DrawLines();
            }
        }
    }
}
