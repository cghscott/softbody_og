using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics.Connectors
{
    public class BungeeConnector : BaseConnector
    {
        private float _springConstant;
        private float _bungeeDamping;

        internal override void getForce(bool isVerlet)
        {
            if (isForceless == false)
            {
                switch (isPartialStatic, isVerlet)
                {
                    case (true, false):
                        BungeeForcePartialStaticInitial();
                        BungeeDragForcePartialStaticInitial();
                        break;
                    case (true, true):
                        BungeeForcePartialStaticVerlet();
                        BungeeDragForcePartialStaticVerlet();
                        break;
                    case (false, false):
                        BungeeForceDynamicInitial();
                        BungeeDragForceDynamicInitial();
                        break;
                    case (false, true):
                        BungeeForceDynamicVerlet();
                        BungeeDragForceDynamicVerlet();
                        break;
                }
            }
        }

        private void BungeeForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            if (overshoot < 0)
            {
                overshoot = 0;
            }
            Vector2 accelerationComponent = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent;
        }

        private void BungeeForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            if (overshoot < 0)
            {
                overshoot = 0;
            }
            Vector2 accelerationComponent = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent;
        }

        private void BungeeForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            if (overshoot < 0)
            {
                overshoot = 0;
            }
            Vector2 accelerationComponent1 = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springConstant * overshoot * -pointToPoint.normalized / endPoint.properties.mass;
            startPoint.verletData.accelerationInitial += accelerationComponent1;
            endPoint.verletData.accelerationInitial += accelerationComponent2;
        }

        private void BungeeForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            if (overshoot < 0)
            {
                overshoot = 0;
            }
            Vector2 accelerationComponent1 = _springConstant * overshoot * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _springConstant * overshoot * -pointToPoint.normalized / endPoint.properties.mass;
            startPoint.kinematicData.acceleration += accelerationComponent1;
            endPoint.kinematicData.acceleration += accelerationComponent2;
        }

        private void BungeeDragForcePartialStaticInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            float relativeAngle = Vector2.Angle(startPoint.kinematicDataOld.velocity, pointToPoint);
            float relativeAngleRadians = relativeAngle * 2 * Mathf.PI / 360;
            Vector2 accelerationComponent = _bungeeDamping * -Mathf.Cos(relativeAngleRadians) * startPoint.kinematicDataOld.velocity.magnitude 
                * pointToPoint.normalized / startPoint.properties.mass;
            if (overshoot >= 0)
            {
                startPoint.verletData.accelerationInitial += accelerationComponent;
            }
        }

        private void BungeeDragForcePartialStaticVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            float relativeAngle = Vector2.Angle(startPoint.verletData.halfstepVelocity, pointToPoint);
            float relativeAngleRadians = relativeAngle * 2 * Mathf.PI / 360;
            Vector2 accelerationComponent = _bungeeDamping * -Mathf.Cos(relativeAngleRadians) * startPoint.verletData.halfstepVelocity.magnitude 
                * pointToPoint.normalized / startPoint.properties.mass;
            if (overshoot >= 0)
            {
                startPoint.kinematicData.acceleration += accelerationComponent;
            }
        }

        private void BungeeDragForceDynamicInitial()
        {
            Vector2 pointToPoint = endPoint.kinematicDataOld.position - startPoint.kinematicDataOld.position;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 velocityRelative = endPoint.kinematicDataOld.velocity - startPoint.kinematicDataOld.velocity;
            float relativeAngle = Vector2.Angle(velocityRelative, pointToPoint);
            float relativeAngleRadians = relativeAngle * 2 * Mathf.PI / 360;
            Vector2 accelerationComponent1 = _bungeeDamping * Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _bungeeDamping * -Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / endPoint.properties.mass;
            if (overshoot >= 0)
            {
                startPoint.verletData.accelerationInitial += accelerationComponent1;
                endPoint.verletData.accelerationInitial += accelerationComponent2;
            }
        }

        private void BungeeDragForceDynamicVerlet()
        {
            Vector2 pointToPoint = endPoint.verletData.pseudoEndPosition - startPoint.verletData.pseudoEndPosition;
            float overshoot = pointToPoint.magnitude - length;
            Vector2 velocityRelative = endPoint.kinematicDataOld.velocity - startPoint.kinematicDataOld.velocity;
            float relativeAngle = Vector2.Angle(velocityRelative, pointToPoint);
            float relativeAngleRadians = relativeAngle * 2 * Mathf.PI / 360;
            Vector2 accelerationComponent1 = _bungeeDamping * Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / startPoint.properties.mass;
            Vector2 accelerationComponent2 = _bungeeDamping * -Mathf.Cos(relativeAngleRadians) * velocityRelative.magnitude * pointToPoint.normalized / endPoint.properties.mass;
            if (overshoot >= 0)
            {
                startPoint.kinematicData.acceleration += accelerationComponent1;
                endPoint.kinematicData.acceleration += accelerationComponent2;
            }
        }

        public BungeeConnector(PhysicsObject startPointInput, PhysicsObject endPointInput, float lengthInput, float springConstantInput, float bungeeDampingInput)
            : base(startPointInput, endPointInput, lengthInput)
        {
            _springConstant = springConstantInput;
            _bungeeDamping = bungeeDampingInput;
        }
    }
}