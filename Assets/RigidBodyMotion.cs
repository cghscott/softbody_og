using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    internal struct Corner
    {
        internal Vector2 position;
        internal float relativeAngleRadians;
        internal float radius;
    }

    public class RigidBodyMotion : PhysicsObject
    {
        private List<Corner> _corners;
        private float _density2D;
        private float _angularDragCoefficient;
        private float _lineWidth;

        public RigidBodyMotion(Vector2 position, bool isStatic, float mass, float gravity, float orientationAngleDegrees, float aerialDragCoefficient,
            float angularDragCoefficient, float width, float height, float lineWidth) : base(position, isStatic, mass, gravity, aerialDragCoefficient)
        {
            _corners = new List<Corner>();
            kinematicData.orientation = orientationAngleDegrees;
            float area = width * height;
            _density2D = properties.mass / area;
            properties.inertia = _density2D * (Mathf.Pow(width, 2) + Mathf.Pow(height, 2)) / 12;
            _angularDragCoefficient = angularDragCoefficient;
            _lineWidth = lineWidth;

            kinematicDataOld = kinematicData;

            // Positive rotations run anti-clockwise, 0 degrees is directly to the right.
            // Currently just using position as centre of mass since that is true for this shape.
            FindCorners(width, height, position);
        }

        private void FindCorners(float width, float height, Vector2 positionOrigin)
        {
            // position labels are relative (can be rotated)

            // top left
            Corner corner0 = new Corner();
            corner0.position.x = kinematicData.position.x - Mathf.Cos(kinematicData.orientation) * width / 2 - Mathf.Sin(kinematicData.orientation) * height / 2;
            corner0.position.y = kinematicData.position.y - Mathf.Sin(kinematicData.orientation) * width / 2 + Mathf.Cos(kinematicData.orientation) * height / 2;
            Vector2 corner0Relative = corner0.position - positionOrigin;
            float corner0relativeAngleDegrees = Vector2.SignedAngle(Vector2.right, corner0Relative) - kinematicData.orientation;
            corner0.relativeAngleRadians = corner0relativeAngleDegrees * Mathf.PI / 180;
            corner0.radius = corner0Relative.magnitude;
            _corners.Add(corner0);

            // top right
            Corner corner1 = new Corner();
            corner1.position.x = kinematicData.position.x + Mathf.Cos(kinematicData.orientation) * width / 2 - Mathf.Sin(kinematicData.orientation) * height / 2;
            corner1.position.y = kinematicData.position.y + Mathf.Sin(kinematicData.orientation) * width / 2 + Mathf.Cos(kinematicData.orientation) * height / 2;
            Vector2 corner1Relative = corner1.position - positionOrigin;
            float corner1relativeAngleDegrees = Vector2.SignedAngle(Vector2.right, corner1Relative) - kinematicData.orientation;
            corner1.relativeAngleRadians = corner1relativeAngleDegrees * Mathf.PI / 180;
            corner1.radius = corner1Relative.magnitude;
            _corners.Add(corner1);

            // bottom right
            Corner corner2 = new Corner();
            corner2.position.x = kinematicData.position.x + Mathf.Cos(kinematicData.orientation) * width / 2 + Mathf.Sin(kinematicData.orientation) * height / 2;
            corner2.position.y = kinematicData.position.y + Mathf.Sin(kinematicData.orientation) * width / 2 - Mathf.Cos(kinematicData.orientation) * height / 2;
            Vector2 corner2Relative = corner2.position - positionOrigin;
            float corner2relativeAngleDegrees = Vector2.SignedAngle(Vector2.right, corner2Relative) - kinematicData.orientation;
            corner2.relativeAngleRadians = corner2relativeAngleDegrees * Mathf.PI / 180;
            corner2.radius = corner2Relative.magnitude;
            _corners.Add(corner2);

            // bottom left
            Corner corner3 = new Corner();
            corner3.position.x = kinematicData.position.x - Mathf.Cos(kinematicData.orientation) * width / 2 + Mathf.Sin(kinematicData.orientation) * height / 2;
            corner3.position.y = kinematicData.position.y - Mathf.Sin(kinematicData.orientation) * width / 2 - Mathf.Cos(kinematicData.orientation) * height / 2;
            Vector2 corner3Relative = corner3.position - positionOrigin;
            float corner3relativeAngleDegrees = Vector2.SignedAngle(Vector2.right, corner3Relative) - kinematicData.orientation;
            corner3.relativeAngleRadians = corner3relativeAngleDegrees * Mathf.PI / 180;
            corner3.radius = corner3Relative.magnitude;
            _corners.Add(corner3);
        }

        public override void RefreshData()
        {
            kinematicDataOld = kinematicData;
            //kinematicData = new KinematicDataRigidBody();        - This might be more robust to future editing than the following
            verletData.accelerationInitial = Vector2.zero;
            kinematicData.acceleration = Vector2.zero;
        }

        public override void GetForce(bool isVerlet)
        {
            if (properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                    GravityForceInitial();
                    AerialDragInitial();
                    AngularDragInitial();
                }
                else
                {
                    GravityForceVerlet();
                    AerialDragVerlet();
                    AngularDragVerlet();
                }
            }
        }

        private void GravityForceInitial()
        {
            verletData.accelerationInitial.y += properties.gravity;
        }

        private void GravityForceVerlet()
        {
            kinematicData.acceleration.y += properties.gravity;
        }

        private void AerialDragInitial()
        {
            Vector2 accelerationDragComponent = properties.aerialDragCoefficient * Mathf.Pow(kinematicDataOld.velocity.magnitude, 2) 
                * -kinematicDataOld.velocity.normalized / properties.mass;
            verletData.accelerationInitial += accelerationDragComponent;
        }

        private void AerialDragVerlet()
        {
            Vector2 accelerationDragComponent = properties.aerialDragCoefficient * Mathf.Pow(verletData.halfstepVelocity.magnitude, 2) 
                * -verletData.halfstepVelocity.normalized / properties.mass;
            kinematicData.acceleration += accelerationDragComponent;
        }

        private void AngularDragInitial()
        {
            float angularDragComponent = _angularDragCoefficient * Mathf.Pow(kinematicDataOld.velocityAngular, 2) 
                * -Mathf.Sign(kinematicDataOld.velocityAngular) / properties.inertia;
            verletData.accelerationInitialRotational += angularDragComponent;
        }

        private void AngularDragVerlet()
        {
            float angularDragComponent = _angularDragCoefficient * Mathf.Pow(verletData.halfstepVelocityAngular, 2) 
                * -Mathf.Sign(verletData.halfstepVelocityAngular) / properties.inertia;
            kinematicData.accelerationRotational += angularDragComponent;
        }

        internal override void ResolveRigidForces(bool isVerlet)
        {
            if (accelerationRigidComponents.Count != 0 && properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                    RigidResolveInitial();
                }
                else
                {
                    RigidResolveVerlet();
                }
            }
            
            accelerationRigidComponents = new List<Vector2>();
        }

        private void RigidResolveInitial()
        {
            Vector2 componentsTotal = Vector2.zero;
            Vector2 componentsAverage;
            foreach (Vector2 component in accelerationRigidComponents)
            {
                componentsTotal += component;
            }
            componentsAverage = componentsTotal / accelerationRigidComponents.Count;
            verletData.accelerationInitial += componentsAverage;
        }

        private void RigidResolveVerlet()
        {
            Vector2 componentsTotal = Vector2.zero;
            Vector2 componentsAverage;
            foreach (Vector2 component in accelerationRigidComponents)
            {
                componentsTotal += component;
            }
            componentsAverage = componentsTotal / accelerationRigidComponents.Count;
            kinematicData.acceleration += componentsAverage;
        }



        internal override void CollisionForce(bool isVerlet)
        {
            if (properties.isStatic == false)
            {
                if (isVerlet == false)
                {
                }
                else
                {
                }
            }
        }



        internal override void CalculatePseudoEndPosition()
        {
            if (properties.isStatic == false)
            {
                verletData.halfstepVelocity = kinematicDataOld.velocity + 0.5f * verletData.accelerationInitial * Time.fixedDeltaTime;
                verletData.pseudoEndPosition = kinematicDataOld.position + verletData.halfstepVelocity * Time.fixedDeltaTime;

                verletData.halfstepVelocityAngular = kinematicDataOld.velocityAngular + 0.5f * verletData.accelerationInitialRotational * Time.fixedDeltaTime;
                verletData.pseudoEndOrientation = kinematicDataOld.orientation + verletData.halfstepVelocityAngular * Time.fixedDeltaTime;
            }
        }

        internal override void CalculateRealEndPosition()
        {
            if (properties.isStatic == false)
            {
                kinematicData.velocity = verletData.halfstepVelocity + 0.5f * kinematicData.acceleration * Time.fixedDeltaTime;
                kinematicData.position = kinematicDataOld.position + kinematicData.velocity * Time.fixedDeltaTime;
                kinematicData.acceleration = (kinematicData.velocity - kinematicDataOld.velocity) / Time.fixedDeltaTime;

                kinematicData.velocityAngular = verletData.halfstepVelocityAngular + 0.5f * kinematicData.accelerationRotational * Time.fixedDeltaTime;
                kinematicData.orientation = kinematicDataOld.orientation + kinematicData.velocityAngular * Time.fixedDeltaTime;
                kinematicData.accelerationRotational = (kinematicData.velocityAngular - kinematicDataOld.velocityAngular) / Time.fixedDeltaTime;

                UpdateCorners();
            }
        }

        // This will also have to be done predictively for collisions unless a boxCollider is used
        private void UpdateCorners()
        {
            float orientationRadians = kinematicData.orientation * Mathf.PI / 180;

            // Some of the calculations in the following loop could potentially be taken out
            for (int i = 0; i < _corners.Count; i++)
            {
                var corner = _corners[i];
                float angle = corner.relativeAngleRadians + orientationRadians;
                corner.position.x = corner.radius * Mathf.Cos(angle) + kinematicData.position.x;
                corner.position.y = corner.radius * Mathf.Sin(angle) + kinematicData.position.y;
                _corners[i] = corner;
            }
        }

        internal override void DrawObject()
        {
            DrawLine(_corners[0], _corners[1]);
            DrawLine(_corners[1], _corners[2]);
            DrawLine(_corners[2], _corners[3]);
            DrawLine(_corners[3], _corners[0]);
        }

        private void DrawLine(Corner start, Corner end)
        {
            GameObject myLine = new GameObject();
            myLine.transform.position = start.position;
            myLine.AddComponent<LineRenderer>();

            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.widthMultiplier = _lineWidth;
            lr.material = new Material(Shader.Find("Sprites/Default"));
            lr.SetPosition(0, start.position);
            lr.SetPosition(1, end.position);

            GameObject.Destroy(myLine, Time.fixedDeltaTime);
        }
    }
}
